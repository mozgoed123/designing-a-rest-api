# Designing a REST API

## Getting started

Watch on Gitlab Pages my [Swagger documentation](https://designing-a-rest-api-mozgoed123-6186caedbf273aea6a16f83f9a38275.gitlab.io/)

Or watch source YAML code here
[TODO_tasks_API.yaml](https://gitlab.com/mozgoed123/designing-a-rest-api/-/blob/main/documentation/TODO_tasks_API.yaml?ref_type=heads)

## Entities

### Task:

#### Attributes:

- id (unique identifier for the task)
- title (title of the task)
- description (description of the task)
- date (when the task was created)
- completed (status of the task)

### User:

#### Attributes:

- email (unique identifier for user)
- password

## Operations:
- Create user
- Authenticate user
- Get all tasks
- Create new task
- Get task by id
- Update task
- Patch task
- Delete Task

## Functional Requirements
### Create user
- Endpoint: PUT /auth
- Description: Create a new user.
- Request Body: JSON object with user details (email, password).
- Response: 201 Created, with the created task in the response body.

### Authenticate user
- Endpoint: POST /auth
- Description: Authenticates existing user.
- Request Body: JSON object with user details (email, password).
- Response: 200 Successs.

### Create Task:
- Endpoint: POST /tasks
- Description: Create a new task.
- Request Body: JSON object with task details (title, description).
- Response: 201 Created, with the created task in the response body.

### Get Task List
- Endpoint: GET /tasks
- Description: Retrieve a paginated list of tasks.
- Query Parameters:
  - page
  - limit
- Response: 200 OK, with a paginated list of tasks in the response body.

### Get Task Details
- Endpoint: GET /task/{id}
- Description: Retrieve details of a specific task.
- Response: 200 OK, with the task details in the response body.

### Update Task
- Endpoint: PUT /task/{id}
- Description: Update the details of a specific task.
- Request Body: JSON object with updated task details.
- Response: 200 OK, with the updated task details in the response body.

### Delete Task
- Endpoint: DELETE /task/{id}
- Description: Delete a specific task.
- Response: 200.

## REST API
[Online documentation](https://designing-a-rest-api-mozgoed123-6186caedbf273aea6a16f83f9a38275.gitlab.io/)

[TODO_tasks_API.yaml](https://gitlab.com/mozgoed123/designing-a-rest-api/-/blob/main/documentation/TODO_tasks_API.yaml?ref_type=heads)


## Non-functional Requirements:

### Richardson Maturity Model:

- Level 1: Use of URI for resources.
- Level 2: Use of HTTP methods (GET, POST, PUT, DELETE) for CRUD operations.
- Level 3: Use of hypermedia controls for navigability.

### Status Codes:
#### Use standard HTTP status codes for request results:
- 200 OK: Successful retrieval or operation.
- 201 Created: Successful resource creation.
- 400 Bad Request: Malformed request or validation error.
- 404 Not Found: Resource not found.

### Errors and Authentication:

- Use standard HTTP status codes for errors (400 for bad requests, 401 for unauthorized).
- Implement token-based authentication for secure operations.

### Pagination:

Paginate task list using the page and limit query parameters.

### Caching:

Implementation for tasks list is not necessary, because it can be changed by a
user at any time.